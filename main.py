#!/usr/bin/env python3

from getpass import getpass
from hashlib import sha512, sha256
from base64 import urlsafe_b64encode

def main():
	pseudo = input("Pseudo: ")
	chaine = input("Chaine specifique: ")
	password = getpass(prompt="Mot de passe: ")
	while password != getpass(prompt="Confirmez: "):
		password = getpass(prompt="Mot de passe: ")
	security = sha512((sha512(password.encode("utf8")).hexdigest() + chaine + sha512(pseudo.encode("utf8")).hexdigest()).encode("utf8")).hexdigest()
	print(urlsafe_b64encode(sha256(security.encode("utf8")).digest()).decode())


if __name__ == "__main__":
	main()
